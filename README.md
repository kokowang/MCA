# FPGA-Trap
Nuclear spectroscopy filter for HPGe detectors.  
Filter is in RedPitaya/FPGA/prj/classic/rtl as part of the Oscilloscope module.  
The oscilloscope is modified, please compile with the provided fpga.c and fpga.h  
Run the oscilloscope at-least once before using the ROOT script to capture data! The FPGA paramaters are set by a module in the oscilloscope.  

DISCLAIMER: This has not been verified to produce accurate readings beyond a signal generator. There are still minor bugs and glitches that may reduce analysis accuracy. See issues for more details.
## How to use:  
Downloads and setup:  
sudo apt-get install make curl xz-utils  
sudo apt-get install libssl-dev device-tree-compiler u-boot-tools  
sudo apt-get install schroot  
sudo apt-get install qemu qemu-user qemu-user-static  
sudo apt-get install lib32z1 lib32ncurses5 lib32bz2-1.0 lib32stdc++6  
https://www.xilinx.com/member/forms/download/xef.html?filename=Xilinx_Vivado_SDK_2016.2_0605_1_Lin64.bin  
sudo ln -s /usr/bin/make /usr/bin/gmake  
git clone https://github.com/kokowang/FPGA-Trap  
cd RP7  
. settings.sh  
mkdir -p dl  
export DL=$PWD/dl  
wget http://downloads.redpitaya.com/ubuntu/redpitaya_ubuntu-latest.tar.gz  
sudo chown root:root redpitaya_ubuntu-latest.tar.gz  
sudo chmod 664 redpitaya_ubuntu-latest.tar.gz  
  
Make config file:  
`/etc/schroot/chroot.d/red-pitaya-ubuntu.conf`  
  
[red-pitaya-ubuntu]  
description=Red Pitaya Debian/Ubuntu OS image  
type=file  
file=								path to file  
users=								user name  
root-users=							admin name  
root-groups=root  
profile=desktop  
personality=linux  
preserve-environment=true  
  
Building:  
make -f Makefile.x86  
schroot -c red-pitaya-ubuntu <<- EOL_CHROOT  
make -f Makefile CROSS_COMPILE="" REVISION=$GIT_COMMIT_SHORT  
EOL_CHROOT  
make -f Makefile.x86 zip  
  
Preparing SD card:  
sudo umount /dev/sdxN  
sudo dd bs=1M if=red_pitaya_image_file.img of=/dev/device_name  
Copy RP7/build.zip/fpga.la onto fpga in SD card.  
  
The image can be found here: https://drive.google.com/open?id=1vskwMvvXtt4ZiF3SPcQkb1anuz3lmAaX

Setting parameters:  
Parameters are currently set during compilation of the oscilloscope module. I plan to move this to an adjustable parameter inside the root script, see issue #4.
Inside fpga.c  
    g_osc_fpga_reg_mem->trap_filt_k = flatpeak;  
    g_osc_fpga_reg_mem->trap_filt_l = risetime;  
    g_osc_fpga_reg_mem->trap_filt_m = (timeconstantfunction);  
    g_osc_fpga_reg_mem->trap_threashold = (threashold, units are 2/65535 volts);  
  
Running:  
cd CPU  
root  
.x c.cpp  



