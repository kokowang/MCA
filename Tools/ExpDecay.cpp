#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <vector>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <unistd.h>
#include <time.h>
#include <iomanip>
using namespace std; 

int main()
{
	ofstream outfile;
	outfile.open("/tmp/exp.csv", ofstream::out);
	setprecision(18);
	if(!(outfile.is_open()))
		cout << "can't open file";
  	for (double i=0; i < 10000; i++){
		if(i < 3500)
			outfile << ((1000000000*exp((0-i)/1000)) + (1000000000*exp((0-i-10000)/1000)) + (200000000*exp((3500-i-10000)/1000)) + rand()%30000000 - rand()%30000000) << endl;
		else
			outfile << ((1000000000*exp((0-i)/1000)) + (1000000000*exp((0-i-10000)/1000)) + (200000000*exp((3500-i-10000)/1000)) + (200000000*exp((3500-i)/1000)) + rand()%30000000 - rand()%30000000) << endl;
	}
	outfile.close();
	return 0;
}

