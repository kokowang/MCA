#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <vector>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <unistd.h>
#include <time.h>
#include<sys/socket.h>   
#include<arpa/inet.h>
#include <iomanip>

using namespace std; 

int main (int argc, char **argv)
{
  string ip;
  string pr;
  string it;
  bool timer = false;
  if (argc > 2){
  	istringstream streama(argv[2]);
	it = streama.str();
	istringstream streamb(argv[1]);
	ip = streamb.str();
  	int x;
  	if (!(streama >> x))
    		cerr << "Invalid number " << argv[1] << '\n';
	else
	{
  		stringstream cmd;
  		cmd<<"ssh root@"+ip+" '/boot/bin/reg 0x40140000 "<<it<<"'";
		//cmd<<"ssh root@"+ip+" '/tmp/a.out 0x40140000 "<<it<<"'";
		pr = cmd.str();
		timer = true;
	}
  }else{
	cout << "Invalid input: getfile <ip> <time>";
  }
  /*stringstream com;
  com<<"ssh root@"<<ip<<" rm -f /tmp/data.txt";
  string fsa = com.str();
  system(fsa.c_str());
  system("rm -f /tmp/parsed.txt");
  system("rm -f /tmp/data.txt");
*/
  pid_t pid = fork();

  if(pid == 0){
  	if(timer){
  		system(pr.c_str());
		//cout << "called server";
	}
	return 0;
  }

  if(pid != 0){

	int sock;
    	struct sockaddr_in server;
    	int server_reply[99];
     
    	//Create socket
   	sock = socket(AF_INET , SOCK_STREAM , 0);
    	if (sock == -1)
    	{
        printf("Could not create socket");
    	}
    	puts("Socket created");
     	
    	server.sin_addr.s_addr = inet_addr(ip.c_str());
    	server.sin_family = AF_INET;
    	server.sin_port = htons( 8888 );
 	
	sleep(2);
    	//Connect to remote server
    	if (connect(sock , (struct sockaddr *)&server , sizeof(server)) < 0)
    	{
        	perror("connect failed. Error");
        	return 1;
    	}
     
    	puts("Connected\n");


//----------------------data, recv start ------------------
			
	ofstream outfile;
	remove("/tmp/parsed.txt");
	int recsize = 0;
	int counts = 0;
	unsigned long long ftime;
	stringstream ss;
	string re;
	string rz;
   	
			/*stringstream comb;
  			comb<<"scp root@"<<ip<<":/tmp/data.txt /tmp/data.txt";
  			string fsdsa = comb.str();
  			system(fsdsa.c_str());
			*/
	recsize = 1;
	while (recsize > 0){
		recsize = recv(sock , server_reply , sizeof(server_reply) , 0);
		outfile.open("/tmp/parsed.txt", ofstream::out | ofstream::app);
		for(int i=0;i<recsize/sizeof(int);i++){
			if (counts % 3 == 0){
				ss.str(std::string());
				ss << hex << server_reply[i+1];
				re = ss.str();
				ss.str(std::string());
				ss << hex << server_reply[i+2];
				rz = ss.str();
				cout << re << endl << rz << endl;
				ftime = strtol(re.c_str(), 0, 16) + strtol(rz.c_str(), 0, 16) * pow(2, 32);
				outfile << ftime << endl << server_reply[i] << endl;	
			}
			counts ++;
		}
		outfile.close();
	}
	close(sock);
	return 0;
  }
}

