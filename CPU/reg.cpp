#include <atomic>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <fcntl.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <stdint.h>
#include <sys/socket.h>
#include <arpa/inet.h> 
#include <iostream>
#include <thread>
#include <iostream>
using namespace std;
#define FATAL do { fprintf(stderr, "Error at line %d, file %s (%d) [%s]\n", \
  __LINE__, __FILE__, errno, strerror(errno)); exit(1); } while(0)
 
#define MAP_SIZE 4096UL
#define MAP_MASK (MAP_SIZE - 1)
#define BUFF_SIZE 10000
#define PACKET_SIZE 99
int parse_from_argv(int a_argc, char **a_argv, unsigned long* a_addr, unsigned long* a_time);
uint32_t read_value(uint32_t a_addr/*, FILE *fptr*/);
void send_message(int data[BUFF_SIZE], atomic<int>& mptr, int client_sock, bool& die);
void* map_base = (void*)(-1);

int main(int argc, char **argv) {
	int buffer[BUFF_SIZE];				//ring buffer
	atomic<int> wptr; 
	wptr.store(0, std::memory_order_release);	//write pointer
	bool die = false;				//kill child thread
	int socket_desc , client_sock , c , read_size;
    	struct sockaddr_in server , client;
	
	//Create socket
    	socket_desc = socket(AF_INET , SOCK_STREAM , 0);
    	if (socket_desc == -1)
    	{
        	printf("Could not create socket");
    	}
	int optval = 1;
     	setsockopt(socket_desc, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(int));
    	//Prepare the sockaddr_in structure
    	server.sin_family = AF_INET;
    	server.sin_addr.s_addr = INADDR_ANY;
    	server.sin_port = htons( 8888 );
	int flags = fcntl(socket_desc, F_GETFL);
     	fcntl(socket_desc, F_SETFL, flags | O_NONBLOCK);
    	//Bind
    	if( bind(socket_desc,(struct sockaddr *)&server , sizeof(server)) < 0)
    	{
        //print the error message
        	perror("bind failed. Error");
		shutdown(socket_desc, 2);
        	return 1;
    	}
     
    	//Listen
    	listen(socket_desc , 3);
     
    	//Accept and incoming connection
    	puts("Waiting for incoming connections...");
    	c = sizeof(struct sockaddr_in);
     
    	//accept connection from an incoming client
	sleep(5);
    	client_sock = accept(socket_desc, (struct sockaddr *)&client, (socklen_t*)&c);

    	if (client_sock < 0)
    	{
        	perror("accept failed");
		shutdown(socket_desc, 2);
        	return 1;
    	}
    	puts("Connection accepted");


//-------------------connected, data collection start--------------------------


	int fd = -1;
	int retval = EXIT_SUCCESS;

	if(argc < 2) {
		return EXIT_FAILURE;
	}

	if((fd = open("/dev/mem", O_RDWR | O_SYNC)) == -1) FATAL;

	/* Read from command line */
	unsigned long addr;
	unsigned long addr2;
	unsigned long addr3;
	unsigned long timing;
	parse_from_argv(argc, argv, &addr, &timing);
	time_t endwait;
   	time_t start = time(NULL);
   	time_t seconds = timing;
   	endwait = start + seconds;
	addr2 = strtoul("0x40140004", 0, 0);
	addr3 = strtoul("0x40140008", 0, 0);
	/* Map one page */
	map_base = mmap(0, MAP_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, addr & ~MAP_MASK);
	if(map_base == (void *) -1) FATAL;

	if (addr != 0) {
		//FILE *fptr;
		unsigned long readval = 0;
		unsigned long timereg1 = 0;
		unsigned long timereg2 = 0;
		unsigned long prev1 = 0;
		unsigned long prev2 = 0;
		std::thread worker(send_message, std::ref(buffer), std::ref(wptr), client_sock, std::ref(die)); 
		//fptr = fopen("/tmp/data.txt", "a");
		while (start < endwait){
			readval = read_value(addr/*, fptr*/);
			timereg1 = read_value(addr2/*, fptr*/);
			timereg2 = read_value(addr3/*, fptr*/);
			if (timereg1 != prev1 || timereg2 != prev2){
				prev1 = timereg1;
				prev2 = timereg2;
				buffer[wptr%BUFF_SIZE] = readval;
				wptr.fetch_add(1 , std::memory_order_release);
				buffer[wptr%BUFF_SIZE] = timereg1;
				wptr.fetch_add(1 , std::memory_order_release);
				buffer[wptr%BUFF_SIZE] = timereg2;
				wptr.fetch_add(1 , std::memory_order_release);
				/*
				fprintf(fptr, "0x%08x\n", readval);
				fprintf(fptr, "0x%08x\n", timereg1);
				fprintf(fptr, "0x%08x\n", timereg2);
				fflush(fptr);
				*/
			}
			start = time(NULL);	
			usleep(100);
		}
		die = true;		//No more data, killing child process
		sleep(1);
	}

	shutdown(client_sock, 2);
	shutdown(socket_desc, 2);
	//Error checking
	if (map_base != (void*)(-1)) {
		if(munmap(map_base, MAP_SIZE) == -1) FATAL;
		map_base = (void*)(-1);
	}

	if (map_base != (void*)(-1)) {
		if(munmap(map_base, MAP_SIZE) == -1) FATAL;
	}
	if (fd != -1) {
		close(fd);
	}
	while (die){			//Wait until child process is done
		sleep(1);
	}
	return retval;
}

void send_message(int data[BUFF_SIZE], atomic<int> &mptr, int client_sock, bool& die)
{
   	int client_message[PACKET_SIZE];
	int wptr = 0;
	int rptr = 0;
	int iptr = 0;
	bool tester = true;
	while ((!die) || rptr < iptr)	//If signaled to stop, finish transmitting buffered data
	{
		iptr = mptr.load(std::memory_order_acquire);
		while (rptr < iptr)
		{
			client_message[wptr % PACKET_SIZE] = data[rptr % BUFF_SIZE];
			if (wptr % PACKET_SIZE == (PACKET_SIZE - 1))		//Once array is full, send packet
				write(client_sock , client_message , sizeof(client_message));
			rptr ++;
			wptr ++;
		}
	}
	if (wptr % PACKET_SIZE != 0)
		write(client_sock , client_message , sizeof(int) * ((wptr - 1) % PACKET_SIZE));	//Send last partial packet
	die = false;
}

uint32_t read_value(uint32_t a_addr/*, FILE *fptr*/) {
	void* virt_addr = map_base + (a_addr & MAP_MASK);
	uint32_t read_result = 0;
	read_result = *((uint32_t *) virt_addr);
	return read_result;
}

int parse_from_argv(int a_argc, char **a_argv, unsigned long* a_addr, 
	unsigned long* a_time) {
	*a_time = strtoul(a_argv[2], 0, 0);
	*a_addr = strtoul(a_argv[1], 0, 0);
	return 0;
}


