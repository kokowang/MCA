#include <stdlib.h>
#include "/usr/local/build/root-6.10.06/include/TRint.h"
#include <string>
#include <time.h>
#include <stdio.h>
#include <fstream>
#include <iostream>
#include <sstream>
using namespace std;

void c()
{
    string buffer = "/tmp/parsed.txt";
    int seconds = 5;
    string cmd;
    string ip;
    cout << "ip of daq:";
    cin >> ip;
    cout << "capture time:";
    cin >> seconds;
    pid_t pid = fork();
    if (pid != 0)
    {
        stringstream command;
        command << "/home/ywang/Desktop/FPGA-Trap/Build/getfile " << ip <<" "<< seconds;
        cmd = command.str();
        system(cmd.c_str());
    }
    if (pid == 0)
    {
        system("rm -f /tmp/parsed.txt");
	TCanvas* c1 = new TCanvas("c1", "c1", 110, 0, 1000, 1000);
	TCanvas* c2 = new TCanvas("c2", "c2", 1110, 0, 1000, 1000);
	c1->cd();
        gStyle->SetCanvasPreferGL(true);
	gStyle->SetOptStat(0);
	c2->cd();
        gStyle->SetCanvasPreferGL(true);
	gStyle->SetOptStat(0);
        ifstream inFile;
        double x;
        double y;
        double elapsed = 0;
	double between = 0;
        inFile.open(buffer);
        int err = 0;
	sleep(5);
        while (!inFile && err < 500)
        {
            err++;
            usleep(10000);
            inFile.open(buffer);
        }
        if (err >= 499)
            cerr << "Can not open file";
        else
        {
            x = 0;
            while (x == 0)
            {
                inFile >> x;
                inFile >> y;
		inFile.clear();
            }
	    x = x/125000000;
	    y = y/32768;
	    c1->cd();
            TH2D* h1 = new TH2D("h1", "Scatter (X = Time(s), Y = Energy (J/C)", 1000, x, x + (seconds), 1000, 0, 1);
	    h1->Fill(x,y);
	    h1->Draw();
	    c2->cd();
	    TH1D* h2 = new TH1D("h2", "Histogram (Energy)", 100, 0, 1);
	    h2->Fill(y);
	    //h2->SetOption("Lego4");
	    h2->Draw();
            time_t start = time(0);
            int i = 0;
            double xmin = 0;
            double xmax = 0;
            long ymin = 0;
            long ymax = 0;
            while (elapsed < seconds + 5)
            {
                time_t end = time(0);
                elapsed = difftime(end, start);
                while (inFile >> x)
                {
                    inFile >> y;
                    h1->Fill(x/125000000, y/32768);
		    h2->Fill(y/32768);
                    i++;
                    if (elapsed > (between + 0.10))
                    {
			between = elapsed;
			c1->cd();
			c2->cd();
			gPad->Update();
                        gPad->Modified();
                    }
                }
                inFile.clear();
		c1->cd();
                gPad->Update();
                gPad->Modified();
		c2->cd();
		gPad->Update();
                gPad->Modified();
                // usleep(5000);
            }
        }
    }
}

#ifndef __CINT__
int main(int argc, char** argv)
{
    gApplication = new TRint("a", &argc, argv);
    c();
    gApplication->Run();
}
#endif
