module tfilter (clk, in, k, l, m, res, y);

	input clk;					//clock
	input [13:0] in;					//input waveform
	input [13:0] k;					//K delay constant
	input [13:0] l;					//L delay constant
	input [15:0] m;					//Time constant
	input res;					//reset active high
	output reg signed [15:0] y;				//output waveform

	reg [14:0] reset = 0;				//reset timer
	reg signed [13:0] buffer;			//f(t)
	reg signed [13:0] kdeli;			//f(t + k)
	reg signed [14:0] aki;				//f(t) - f(t + k)->g(t)
	reg signed [14:0] ldeli;			//g(t + l)
	reg signed [15:0] akli; 			//g(t) - g(t + l)->h(t)
	reg signed [63:0] aklmi;			//m * h(t)
	reg signed [63:0] bi;				//sigma(h(t))
	reg signed [63:0] ci;				//m * h(t) + sigma(h(t))->(t)
	reg signed [63:0] oi;				//sigma(i(t))
	reg signed [20:0] oxi;				//trap(t)
	reg signed [20:0] opi;				//trap(t + k)
	reg signed [20:0] oqi;				//trap(t) - trap(t + k)
	reg [15:0] mm;					//m
	reg [13:0] ll;					//l
	reg [13:0] kk;					//k
	reg [44:0] nn;					//magnitude correction
	reg [14:0] cnt;					//RAM R+W pointer

	wire [13:0] kdel;
	wire [14:0] ldel;				
	wire [20:0] op;

    	ram1 r0 (clk, 1'd1, 1'd1, 1'd1, cnt, cnt-kk, buffer, kdel);
	ram2 r1 (clk, 1'd1, 1'd1, 1'd1, cnt, cnt-ll, aki, ldel);
	ram3 r2 (clk, 1'd1, 1'd1, 1'd1, cnt, cnt-kk, oxi, op);
	
	always @ (m or l or k) begin
		mm = m;
		ll = l-1;
		kk = k-1;
		nn <= 46'd35184372088832/(mm*(ll+1));
	end
	always @ (posedge clk) begin
		cnt = cnt + 1;				//move pointer
		if (res == 1)
			reset = 50 + (2**14);		//time to reset
		if (reset > 0) begin
			buffer = 0;
			kdeli  = 0;	
			aki    = 0;
			ldeli  = 0;
			akli   = 0;				
			bi     = 0;
			ci     = 0;
			oi     = 0;
			oxi    = 0;
			opi    = 0;
			oqi    = 0;
			y      = 0;
			reset  = reset - 1;
		end
		else begin
			buffer <= in;
			kdeli  <= kdel;	
			aki    <= buffer 	- 	kdeli;
			ldeli  <= ldel;
			akli   <= aki 		- 	ldeli;
			aklmi  =  akli          *       $signed(mm);
			bi     =  bi 		+ 	akli;
			ci     <= aklmi		+ 	bi;
			oi     =  oi 		+ 	ci;
			oxi    <= ($signed(oi)*nn) 		>> 	38;
			opi    <= op;
			oqi    <= $signed($signed(oxi) 		- 	$signed(opi));
			y      <= oqi				>> 5;
		end
	end	
endmodule



//RAM for K delay
module ram1 (clk, ena, enb, wea, addra, addrb, dia, dob);
	input clk,ena,enb,wea;
	input [14:0] addra,addrb;
	input [13:0] dia;
	output [13:0] dob;
	reg[13:0] ram [2**14-1:0];
	reg[13:0] doa,dob;
	always @(posedge clk) begin
		if (ena) begin
			if (wea)
				ram[addra] <= dia;
		end
	end
	always @(posedge clk) begin
		if (enb)
			dob <= ram[addrb];
	end
endmodule

//RAM for L delay 
module ram2 (clk, ena, enb, wea, addra, addrb, dia, dob);
	input clk,ena,enb,wea;
	input [14:0] addra,addrb;
	input [14:0] dia;
	output [14:0] dob;
	reg[14:0] ram [2**14-1:0];
	reg[14:0] doa,dob;
	always @(posedge clk) begin
		if (ena) begin
			if (wea)
				ram[addra] <= dia;
		end
	end
	always @(posedge clk) begin
		if (enb)
			dob <= ram[addrb];
	end
endmodule



//RAM for inversion
module ram3 (clk, ena, enb, wea, addra, addrb, dia, dob);
	input clk,ena,enb,wea;
	input [14:0] addra,addrb;
	input [20:0] dia;
	output [20:0] dob;
	reg[20:0] ram [2**14-1:0];
	reg[20:0] doa,dob;
	always @(posedge clk) begin
		if (ena) begin
			if (wea)
				ram[addra] <= dia;
		end
	end
	always @(posedge clk) begin
		if (enb)
			dob <= ram[addrb];
	end
endmodule

