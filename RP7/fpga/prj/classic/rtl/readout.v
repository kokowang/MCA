
module readout (clk, in, thresh, ll, data, reset, timerz);
  input clk, reset;
  input signed [15:0] in, thresh;
  input [15:0] ll;
  output reg [15:0] data;
  reg [15:0] cnt;
  reg arm;
  output reg [63:0] timerz = 0;
  reg [63:0] timer = 0;
  
  always @ (posedge clk) begin
    if (reset != 0) begin
      cnt = 0;
      arm = 0;
      data = 0;
      timer = 0;
    end else begin
      timer = timer + 1;
      if(in >= thresh) begin
        arm <= 1;
      end
      if(in < 0 && arm == 1) begin
        arm <= 0;
        if (cnt > 0) begin
        end else begin
          cnt = (ll/2) + 10;
        end
      end else begin
        if(cnt > 0) begin
          if(cnt == 1) begin
            data = 0-in;
	    timerz = timer; 
          end
          cnt = cnt - 1;
        end
      end
    end
  end
endmodule


